FROM debian:testing

RUN apt update

RUN apt install -y \
    python3 python3-pip python3-venv \
    pandoc \
    biber latexmk make \
    git git-lfs \
    texlive-latex-base \
    texlive-latex-extra \
    sqlite3 \
    python3-dev libbz2-1.0 xz-utils zlib1g libxml2-dev libxslt1-dev libpopt0 libmagic-dev \
    && rm -fr /var/cache/*

WORKDIR /app
